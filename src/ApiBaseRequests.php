<?php

namespace MdelaRiva\ApiRequests;

use ReflectionClass;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Pluralizer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use MdelaRiva\ApiRequests\Libraries\Hook\Hook;
use MdelaRiva\ApiRequests\Libraries\Hook\HookType;

trait ApiBaseRequests
{
    /**
    * Trait initialized flag
    *
    * @var bool
    */
    private $initializedApiController = FALSE;

    /**
     * Define hooks to register
     *
     * @return void
     */
    protected function registerApiHooks(): void {}

    /**
     * Initializes trait configuration
     *
     * @return void
     */
    private function initializeApiTrait()
    {
        // already initialize?
        if( $this->initializedApiController === true )
        {
            return;
        }
        // initialize hooks
        Hook::reset();
        $this->registerApiHooks();
        // hook - before
        Hook::fire( HookType::InitBefore );
        // initialize trait configuration
        $this->initializedApiController = true;
        $reflect = new ReflectionClass( $this );
        if( empty($this->model) )
        {
            $modelPath = $this->modelPath ?? '\App\Models\\';
            $modelName = $modelPath . str_replace( 'Controller', '', $reflect->getShortName() );
            $this->model = $modelName;
        }
        if( empty($this->resource) )
        {
            if( $this->model )
            {
                $reflect = new ReflectionClass( $this->model );
            }
            $resourcePath = $this->resourcePath ?? '\App\Http\Resources\\';
            $resourceName = $resourcePath . str_replace( 'Controller', '', $reflect->getShortName() ) . 'Resource';
            $this->resource = $resourceName;
        }
        // hook - after
        Hook::fire( HookType::InitAfter );
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $this->initializeApiTrait();
        // hook - before
        Hook::fire( HookType::IndexBefore, $request );
        // default limit
        $limitAmount = $this->limitAmount ?? 20;
        $model = $this->model;
        $modelTable = (new $model)->getTable();
        $query      = $model::query();
        if( $modelTable )
        {
            $query->select( $modelTable . '.*' );
        }
        // > where
        if( $request->all() )
        {
            $this->indexWhereBuildSet( $query, $request );
        }
        // < where
        // > order by
        if( $request->filled( 'orderBy' ) )
        {
            $orderBy = $request->get('orderBy');
            // > related table
            $matches = [];
            preg_match( '/^(.+?)\./i', $orderBy, $matches );
            if( $matches )
            {
                $tableSingular = $matches[1];
                $tablePlural = Pluralizer::plural( $tableSingular );
                $orderBy = str_replace( $tableSingular . '.', $tablePlural . '.', $orderBy);
                $query->join( $tablePlural, $modelTable . '.' . $tableSingular . '_id', '=', $tablePlural . '.id' );
            }
            // < related table
            // asc, desc
            if( $request->filled( 'orderByDir' ) )
            {
                if( stripos( $orderBy, 'field(' ) === 0 )
                {
                    $query->orderByRaw( stripslashes($orderBy) . ' ' . $request->get( 'orderByDir' ) );
                }
                else
                {
                    $query->orderBy( $orderBy, $request->get( 'orderByDir' ) );
                }
            }
            else
            {
                if( stripos( $orderBy, 'field(' ) === 0 )
                {
                    $query->orderByRaw( stripslashes($orderBy) );
                }
                else
                {
                    $query->orderBy( $orderBy );
                }
            }
        }
        else if( isset($this->indexDefaultOrderBy) )
        {
            if( isset($this->indexDefaultOrderByDir) )
            {
                $query->orderBy( $this->indexDefaultOrderBy, $this->indexDefaultOrderByDir );
            }
            else
            {
                $query->orderBy( $this->indexDefaultOrderBy );
            }
        }
        // < order by
        // hook - pre run
        Hook::fire( HookType::IndexPreRun, $query );
        // limit
        if( $request->filled( 'limit' ) )
        {
            $limitAmount = $request->get( 'limit' );
        }
        $itemsList = [];
        if( $limitAmount != -1 )
        {
            $itemsList = $query->paginate( $limitAmount );
        }
        else
        {
            $itemsListRaw = $query->get();
            if( $itemsListRaw->count() )
            {
                $itemsList = new LengthAwarePaginator(
                    $itemsListRaw,
                    $itemsListRaw->count(),
                    $itemsListRaw->count(),
                    1,
                    [
                        'path'  => request()->url(),
                        'query' => request()->query(),
                    ]
                );
            }
        }
        // hook - after
        Hook::fire( HookType::IndexAfter );

        return ($this->resource)::collection( $itemsList ? $itemsList : [] );
    }

    /**
     * Query where builder for index
     *
     * @param  mixed  $query
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function indexWhereBuildSet( $query, Request $request )
    {
        $this->initializeApiTrait();
        //
        self::buildWhereFromRequest( $this->model, $query, $request->all() );
    }

    /**
     * Query where builder helper
     *
     * @param  string  $modelName
     * @param  mixed  $query
     * @param  array  $requestWhere
     * @return void
     */
    public static function buildWhereFromRequest( string $modelName, $query, array $requestWhere = [] )
    {
        if( !$requestWhere )
        {
            return null;
        }
        $itemWhere = new $modelName();
        $tableName = $itemWhere->getTable();
        $validations = $modelName::getValidations();
        $fillableFields = $itemWhere->getFillable();
        $dateFields = $itemWhere->getDates();
        $usesTimestamps = $itemWhere->usesTimestamps();
        //
        foreach( $requestWhere as $whereField => $whereValueRaw )
        {
            $whereValue = $whereValueRaw;
            $whereOperator = '=';
            $isDate = false;
            $matches = null;
            preg_match('/_([^_].)$/i', $whereField, $matches);
            if( $matches )
            {
                switch( $matches[1] )
                {
                    case 'gt':
                        // greater than
                        $whereOperator = '>';
                        break;
                    case 'ge':
                        // greater or equal than
                        $whereOperator = '>=';
                        break;
                    case 'lt':
                        // less than
                        $whereOperator = '<';
                        break;
                    case 'le':
                        // less or equal than
                        $whereOperator = '<=';
                        break;
                    case 'ct':
                        // contains
                        $whereOperator = 'like';
                        $whereValueLike = '%' . $whereValue . '%';
                        $whereValue = [
                            $whereValueLike,
                            mb_strtolower( $whereValueLike),
                            mb_strtoupper( $whereValueLike),
                            mb_convert_case($whereValueLike, MB_CASE_TITLE, 'UTF-8'),
                        ];
                        break;
                    case 'pr':
                        // prepends
                        $whereOperator = 'like';
                        $whereValueLike = $whereValue . '%';
                        $whereValue = [
                            $whereValueLike,
                            mb_strtolower( $whereValueLike),
                            mb_strtoupper( $whereValueLike),
                            mb_convert_case($whereValueLike, MB_CASE_TITLE, 'UTF-8'),
                        ];
                        break;
                    case 'ap':
                        // appends
                        $whereOperator = 'like';
                        $whereValueLike = '%' . $whereValue;
                        $whereValue = [
                            $whereValueLike,
                            mb_strtolower( $whereValueLike),
                            mb_strtoupper( $whereValueLike),
                            mb_convert_case($whereValueLike, MB_CASE_TITLE, 'UTF-8'),
                        ];
                        break;
                }
                if( $whereOperator != '=' )
                {
                    $whereField = preg_replace( '/(_[^_]+)$/i', '', $whereField );
                }
            }

            if( in_array( $whereField, $fillableFields ) )
            {
                // is this a valid field?
                Validator::make( [ $whereField => $whereValueRaw ], $validations )->validate();
                if( in_array( $whereField, $dateFields ) )
                {
                    $isDate = true;
                }
            }
            elseif( $whereField == 'id' )
            {
                // exception field: id
                Validator::make( [ 'id' => $whereValueRaw ], [ 'id' => 'integer' ] )->validate();
            }
            elseif( $usesTimestamps )
            {
                // is this a timestamp field?
                switch( $whereField )
                {
                    case $itemWhere->getCreatedAtColumn():
                    case $itemWhere->getUpdatedAtColumn():
                        $isDate = true;
                        Validator::make( [ $whereField => $whereValueRaw ], [ $whereField => 'date' ] )->validate();
                        break;
                    default:
                        continue 2;
                }
            }
            else
            {
                // trash or some field for another thing
                continue;
            }
            if( $isDate )
            {
                if( strlen($whereValue) <= 10 )
                {
                    // short date
                    $query->whereDate( ( $tableName ? $tableName . '.' : '' ) . $whereField, $whereOperator, $whereValue );
                }
                else
                {
                    $query->where( ( $tableName ? $tableName . '.' : '' ) . $whereField, $whereOperator, $whereValue );
                }
            }
            else
            {
                if( is_array($whereValue) )
                {
                    $query->where( function ($query) use ($whereField, $whereOperator, $whereValue, $tableName){
                        foreach( array_values( $whereValue ) as $whereValueIndex => $whereValueItem )
                        {
                            if( $whereValueIndex == 0 )
                            {
                                $query->where( ( $tableName ? $tableName . '.' : '' ) . $whereField, $whereOperator, $whereValueItem);
                            }
                            else
                            {
                                $query->orWhere( ( $tableName ? $tableName . '.' : '' ) . $whereField, $whereOperator, $whereValueItem);
                            }
                        }
                    } );
                }
                else
                {
                    $query->where( ( $tableName ? $tableName . '.' : '' ) . $whereField, $whereOperator, $whereValue );
                }
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $this->initializeApiTrait();
        // hook - before
        Hook::fire( HookType::StoreBefore, $request );
        //
        $validations = ($this->model)::getValidations();

        foreach( ($this->model)::getValidationsRequired() as $validationRequired )
        {
            if( !isset($validations[$validationRequired]) )
            {
                $validations[$validationRequired] = '';
            }
            if( is_array($validations[$validationRequired]) )
            {
                $validations[$validationRequired][] = 'required';
            }
            else
            {
                $validations[$validationRequired] = 'required|' . $validations[$validationRequired];
            }
        }

        $this->validate($request, [
            'data'      => 'required|array',
        ]);
        $requestItem = $request['data'];
        Validator::make( $requestItem, $validations )->validate();

        $item = new $this->model();
        $item->fill( $requestItem );
        $result = $item->save();

        // hook - after
        Hook::fire( HookType::StoreAfter, $result, $item );

        return response()->json( new $this->resource( $item ) );
    }

    /**
     * Display the specified resource.
     *
     * @param  mixed  $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        $this->initializeApiTrait();
        //
        $item = ($this->model)::findOrFail( $id );

        return response()->json( new $this->resource( $item ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        $this->initializeApiTrait();
        // hook - before
        Hook::fire( HookType::UpdateBefore, $request );
        //
        $item = ($this->model)::findOrFail( $id );
        //
        $validations = ($this->model)::getValidations();
        //
        $this->validate($request, [
            'data'      => 'required|array',
        ]);
        $requestItem = $request['data'];
        Validator::make( $requestItem, $validations )->validate();
        //
        $item->fill( $requestItem );
        $result = $item->save();
        // hook - after
        Hook::fire( HookType::UpdateAfter, $result, $item );

        return response()->json( new $this->resource( $item ) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  mixed  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        $this->initializeApiTrait();
        //
        $item = ($this->model)::findOrFail( $id );
        // hook - before
        Hook::fire( HookType::DeleteBefore, $item );
        //
        $item->delete();
        // hook - after
        Hook::fire( HookType::DeleteAfter, $id );

        return response()->json('', 204);
    }
}
