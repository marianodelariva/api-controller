<?php

namespace MdelaRiva\ApiRequests\Libraries\Hook;

enum HookType
{
    case InitBefore;
    case InitAfter;

    case IndexBefore;
    case IndexPreRun;
    case IndexAfter;

    case StoreBefore;
    case StoreAfter;

    case UpdateBefore;
    case UpdateAfter;

    case DeleteBefore;
    case DeleteAfter;
}