<?php
namespace MdelaRiva\ApiRequests\Libraries\Hook;

use Closure;
use MdelaRiva\ApiRequests\Libraries\Hook\HookType;

class Hook
{
    /**
     * List of registered hooks
     *
     * @var array
     */
    private static $hooks = [];

    /**
     * Register a hook
     *
     * @param \MdelaRiva\ApiRequests\Libraries\Hook\HookType $hookName
     * @param Closure $hookAction
     * @return void
     */
    public static function register( HookType $hookName, Closure $hookAction ): void
    {
        self::$hooks[$hookName->name] = $hookAction;
    }

    /**
     * Calls a hooks closure
     *
     * @param \MdelaRiva\ApiRequests\Libraries\Hook\HookType $hookName
     * @param mixed $args
     * @return mixed
     */
    public static function fire( HookType $hookName, ...$args )
    {
        if( empty(self::$hooks[$hookName->name]) )
        {
            return;
        }
        return call_user_func_array( self::$hooks[$hookName->name], $args);
    }

    /**
     * Reset list of hooks
     *
     * @return void
     */
    public static function reset()
    {
        self::$hooks = [];
    }
}